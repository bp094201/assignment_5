package Domain;
import java.util.*;
import java.util.Map.Entry;

public class Shopping
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		Customer cust=new Customer();
		Product prod=new Product();
		Cart ct=new Cart();
		int choice;
		while(true)
		{
			System.out.println("1:Shopping\n2:Cart\n3:Exit\nEnter your choice");
			choice=scan.nextInt();
			switch(choice)
			{
				case 1: 
							System.out.println("Available products are : ");
							prod.showAvailableProducts();
							System.out.println("Brand\tPrice\t\tQuantity");
							Iterator<Entry<String, List<Double>>> it=prod.Groceries.entrySet().iterator();
							while(it.hasNext())
							{
								Map.Entry<String,List<Double>> e=(Map.Entry<String,List<Double>>)it.next();
								System.out.println(e.getKey()+"\t"+e.getValue().get(0)+"\t\t"+e.getValue().get(1));
							}
							System.out.println("Enter Name of the product");
							prod.name=scan.next();
							System.out.println("Enter quantity of the product");
							prod.qty=scan.nextDouble();
							if(prod.qty<=prod.Groceries.get(prod.name).get(1))
							{
								if(prod.Groceries.containsKey(prod.name))
								{
									prod.price=prod.Groceries.get(prod.name).get(0);
								}
								System.out.println("Do you want to add in cart? Yes or No?");
								String cho=scan.next();
								if(cho.equalsIgnoreCase("Yes"))
								{
									ct.prodList.put(prod.name, Arrays.asList(prod.price,prod.qty,(prod.price*prod.qty)));
									Double x=prod.Groceries.get(prod.name).get(1);
									prod.Groceries.get(prod.name).set(1,x-prod.qty);
									System.out.println("Your items are added into cart");
								}
							}
							else
							{
								System.out.println("Your required quantity is not in stock");
							}
							break;
		        case 2: ct.displayInvoice();
				        break;
                case 3: scan.close();
				        System.exit(choice);
				        break;
                default: System.out.println("Invalid Choice");
			}
			
		}
	}
}

