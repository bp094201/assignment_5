package Domain;

import java.util.*;
import java.util.Map.Entry;

public class Cart
{
	int id;
	HashMap<String,List<Double>>prodList=new HashMap<String,List<Double>>();
	double total=0;
	public void displayInvoice()
	{
		Iterator<Entry<String, List<Double>>> itP=prodList.entrySet().iterator();
		System.out.println("-------------Your Cart---------------------------");
		System.out.println("Name\tPrice\t\tQuantity\tSub-total");
		while(itP.hasNext())
		{
			Map.Entry<String, List<Double>>pm=(Map.Entry<String, List<Double>>)itP.next();
			System.out.println(pm.getKey()+"\t"+pm.getValue().get(0)+"\t\t"+pm.getValue().get(1)+"\t\t"+pm.getValue().get(2));
			total+=pm.getValue().get(2);
		}
		System.out.println("\nTotal Amount:"+total+"\n");
	}
}

